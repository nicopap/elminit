#![feature(try_from,box_patterns,box_syntax,type_ascription)]

#[cfg(test)] #[macro_use] extern crate  pretty_assertions;
#[macro_use] extern crate quick_error;
pub extern crate polytype;
extern crate fxhash;

#[macro_use]
pub mod expression;
#[macro_use]
pub mod eval;
pub mod htmlify;
pub use self::expression::{Expression,Pattern};

