use super::taint::Runtime;
use super::view::{TaintMsg,TaintView};
use super::update::TaintUpdate;

fn judge(
    update: Value<TaintUpdate>,
    msgs: Vec<Value<TaintMsg>>,
    pt: &Runtime<TaintUpdate>,
) -> bool {
    fn judge_i(update: Value<TaintUpdate>, msg: &Value<TaintMsg>) -> bool {
        let id = Box::new(|_,_,v|v).as_ref();
        let update = update.clone();
        let evaluation =
            update.apply(id, msg, pt)
                .apply(id, Value::Unk(TaintUpdate::Update), pt);
        evaluation == TaintUpdate::View
    }
    msgs.iter().any(|msg| judge_i(update, msg))
}

fn gather(view: Value<TaintView>, pt: &Runtime<TaintView>)
    -> Vec<Value<TaintMsg>>
{
    let id = Box::new(|_,_,v|v);
    let view_taint = view.apply(id, Value::Unk(TaintView::View),pt);
    view_taint.unwrap().mark().into_iter().collect()
}

fn verify<N>(program: Expression<N>) -> bool {
    let runtime = // ...;
    let context = // ...;
    let id = Box::new(|_,_,v|v);
    let (update, view) =
        taint::evaluate(id, program, context, runtime).unwrap();
    !judge(update,gather(view))
}
