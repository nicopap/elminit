//! The AST of the enriched λ-calculus, with helper types for generic marking.

use std::fmt::{self,Display};

#[macro_export]
macro_rules! elminit {
    (λ $name:ident . $($body:tt)+) => (
        $crate::Expression::Lambda {
            binding: stringify!($name).to_owned(),
            body: Box::new(elminit!($($body)+))
        }
    );
    (letrec $rec_name:ident = λ $binding:ident . {$($body:tt)+} in $($over:tt)+) => (
        $crate::Expression::Letrec {
            rec_name: stringify!($rec_name).to_owned(),
            binding: stringify!($binding).to_owned(),
            body: Box::new(elminit!($($body)+)),
            over: Box::new(elminit!($($over)+))
        }
    );
    (let ($($binding:tt)+) = {$($value:tt)+} in $($over:tt)+) => (
        $crate::Expression::Let {
            binding: elminit!(@pat $($binding)+),
            value: Box::new(elminit!($($value)+)),
            over: Box::new(elminit!($($over)+))
        }
    );
    (case ($($over:tt)+) of {$( ($($pat:tt)+) -> {$($expr:tt)+} ),+ }) => (
        $crate::Expression::Case {
            over: Box::new(elminit!($($over)+)),
            branches: vec![ $( (
                    elminit!(@pat $($pat)+),
                    elminit!($($expr)+)
            ) ),* ]
        }
    );
    (($($expr:tt)+)) => (
        elminit!($($expr)+)
    );
    ($name:ident) => (
        $crate::Expression::Ident(
            stringify!($name).to_owned()
        )
    );
    (@pat _) => (
        $crate::Pattern::Wildcard
    );
    (@pat ^ $constr:ident $( ($($arg:tt)+) )* ) => (
        $crate::Pattern::Constructor {
            name: stringify!($constr).to_owned(),
            arguments: vec![ $( elminit!(@pat $($arg)+) ),* ]
        }
    );
    (@pat $name:ident) => (
        $crate::Pattern::Ident(stringify!($name).to_owned())
    );
    (@pat $val:expr) => (
        $crate::Pattern::Literal
    );
    ($lval:tt $rval:tt $($rem:tt)+) => (
        elminit!(($lval $rval) $($rem)+)
    );
    ($fun:tt  $arg:tt) => (
        $crate::Expression::Application {
            function: Box::new(elminit!($fun)),
            argument: Box::new(elminit!($arg))
        }
    );
    ($val:expr) => (
        $crate::Expression::Literal
    );
}


// partial!{/ C Nil} | partial!{x / C Cons} | partial!{x1 x2 / K add}
// {Nil}^C           | λ x . {Cons x}^C     | λx1. λx2. {add x1 x2}^K
#[macro_export]
macro_rules! partial {
    {$name:ident $($rem:tt)*} => (
        $crate::Expression::Lambda {
            binding: stringify!($name).into(),
            body: Box::new(partial!{$($rem)* $name}),
        }
    );
    {/ C $name:ident $($rem:ident)*} => (
        $crate::Expression::Constr {
            name: stringify!($name).into(),
            arguments: vec![$(stringify!($rem).into()),*],
        }
    );
    {/ K $name:ident $($rem:ident)*} => (
        $crate::Expression::Kernel {
            name: stringify!($name).into(),
            arguments: vec![$(stringify!($rem).into()),*],
        }
    );
}

/// An expression in the enriched λ-calculus
/// Generic over the literal `Lit` choosen by the implementer.
/// This very close to the expression defined in module `lambda`, with the
/// addition of the recursive let (`Letrec`) and the case expression (`Case`)
#[derive(Clone,Debug, Hash)]
pub enum Expression<Name> {
    /// A λ-expression (`λx. Expr`)
    Lambda {
        /// The name of the binding (it is `x` in `λx. Expr`)
        binding: Name,
        /// The body (it is `Expr` in `λx. Expr`)
        body: Box<Expression<Name>>,
    },
    Application {
        /// `f` in `f x`
        function: Box<Expression<Name>>,
        /// `x` in `f x`
        argument: Box<Expression<Name>>,
    },
    /// A literal.
    Literal,
    /// A free or bound variable.
    Ident(Name),
    /// An expression where expressions can be bound to variables which
    /// are available in the expression itself.
    Letrec {
        rec_name: Name,
        binding: Name,
        body: Box<Expression<Name>>,
        over: Box<Expression<Name>>,
    },
    Let {
        binding: Pattern<Name>,
        value: Box<Expression<Name>>,
        over: Box<Expression<Name>>,
    },
    /// A deconstructing expression.
    Case {
        over: Box<Expression<Name>>,
        branches: Vec<(Pattern<Name>, Expression<Name>)>,
    },
    Constr {
        name: Name,
        arguments: Vec<Name>,
    },
    Kernel {
        name: Name,
        arguments: Vec<Name>,
    },
}

/// A path through an AST of richlambda::Expression
#[derive(Clone,Debug,Copy)]
pub enum ExPath {
    LBody,
    AppFun,
    AppArg,
    LetLet,
    LetIn,
    CaseOver,
    CaseBranch(u8),
}

impl <Name:PartialEq>Expression<Name> {
    pub fn appear_free(&self, var: &Name) -> bool {
        use self::Expression::*;
        match self {
            Application {ref function, ref argument} =>
                function.appear_free(var) || argument.appear_free(var),

            //Never will be free, is bound :(
            Lambda{ref binding,..} if binding==var => false,
            Lambda{ref body, ..} /*otherwise*/     =>
                body.appear_free(var),
            Ident(ref name) if name == var    => true,
            Ident(_) | Literal => false,
            Case{ref over, ref branches} =>
                over.appear_free(var)
                || branches.iter().any(|(_,expr)| expr.appear_free(var)),

            Let{ref over, ref value, ..} =>
                over.appear_free(var) || value.appear_free(var),
            Letrec{ref over, ref body, ..} =>
                over.appear_free(var) || body.appear_free(var),
            Constr {arguments,..} | Kernel {arguments,..} =>
                arguments.iter().any(|x| x == var),
        }
    }
}

/// `Pattern` describes deconstructing elements of the grammar.
#[derive(Clone,Debug, Hash)]
pub enum Pattern<Name> {
    Literal,
    Ident(Name),
    Constructor {
        name: Name,
        arguments: Vec<Pattern<Name>>,
    },
    Wildcard,
}

impl<Name> Pattern<Name> {
    pub fn into_ident(self) -> Option<Name> {
        if let Pattern::Ident(name) = self {
            Some(name)
        } else {
            None
        }
    }
}

impl<N:Display> fmt::Display for Pattern<N> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Pattern::*;
        match *self {
            Literal =>
                write!(f, ":lit:"),
            Ident( ref name) =>
                write!(f, "{}", name),
            Constructor { ref name, ref arguments } => {
                write!(f, "{}", name)?;
                for arg in arguments.iter() {
                    match *arg {
                        Wildcard =>
                            write!(f," _")?,
                        Literal | Ident(_) =>
                            write!(f," {}", arg)?,
                        Constructor {ref arguments, ..}
                                if !arguments.is_empty() =>
                            write!(f," ({})", arg)?,
                        Constructor {..} =>
                            write!(f," {}", arg)?,
                    };
                }
                Ok(())
            },
            Wildcard =>
                write!(f, "_"),
        }
    }
}

impl<N:Display> fmt::Display for Expression<N> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Expression::*;
        match self {
            Lambda { ref binding, ref body } =>
                write!(f, "λ {}.{}", binding, body),

            Application { ref function, ref argument } => {
                match *function.as_ref() {
                    Lambda {..} | Case {..} | Letrec {..} | Let {..} =>
                        write!(f, "({}) ", function),
                    Application {..} | Literal | Ident(_)
                    | Kernel{..} | Constr{..} =>
                        write!(f, "{} ", function),
                }?;
                match *argument.as_ref() {
                    Lambda{..} | Application{..}
                    | Case{..} | Letrec{..} | Let{..} =>
                        write!(f, "({})", argument),
                    Literal | Ident(_) | Kernel {..}
                    | Constr {..} =>
                        write!(f, "{}", argument),
                }
            },
            Literal =>
                write!(f, ":lit:"),
            Ident(ref name) =>
                write!(f, "{}", name),
            Let { ref binding, ref value, ref over } =>
                write!(f, "let {} = {} in {}", binding, value, over),
            Letrec { ref binding, ref rec_name, ref body, ref over } => {
            write!(f, "letrec {} = λ {} . {} in {}", rec_name, binding,
                   body, over)
            },
            Case { ref over, ref branches } => {
                write!(f, "case {} of\n", over)?;
                for &(ref pattern, ref body) in branches.iter() {
                    write!(f, "    {} -> {}\n", pattern, body)?;
                }
                Ok(())
            },
            Constr { ref name, ref arguments } => {
                write!(f, "{{{}:", name)?;
                for arg in arguments.iter() {
                    write!(f, " {}", arg)?;
                }
                write!(f,"}}^C")
            },
            Kernel { ref name, ref arguments } => {
                write!(f, "{{{}:", name)?;
                for arg in arguments.iter() {
                    write!(f, " {}", arg)?;
                }
                write!(f,"}}^K")
            },
        }
    }
}

