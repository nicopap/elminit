use std::hash::{Hash,Hasher};
use std::fmt;
pub use std::rc::Rc;

use fxhash::FxHashMap;

use super::{Expression,Pattern};
use expression::ExPath;


impl<T:fmt::Debug> fmt::Debug for Value<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Value::*; match *self {
            Closure {ref body, ..} =>
                write!(f, "({})[ctx...]", body),
            ReClosure {ref body, ..} =>
                write!(f, "re({})[ctx...]", body),
            Unk(ref taint) => write!(f, "Unk^{{{:?}}}", taint),
            Structure { ref constructor, ref values } =>
                write!(f, "({} {:?})", constructor, values),
        }
    }
}

impl<T:fmt::Display> fmt::Display for Value<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Value::*; match *self {
            Unk(ref taint)   => write!(f, "Unk^{{{}}}", taint),
            Closure {ref body, ..}   => write!(f, "[[{}]]", body),
            ReClosure {ref body, ..} => write!(f, "[[{}]]", body),
            Structure {ref constructor, ref values} => {
                write!(f, "({}", constructor)?;
                for value in values.iter() {
                    write!(f, " {}", value)?;
                }
                write!(f, ")")
            },
        }
    }
}

pub type RExpression = Expression<String>;
pub type RPattern= Pattern<String>;

pub type Context<T> = FxHashMap<String,Value<T>>;

// context!( &runtime;
//   "Nil" => {Nil}^C,
//   "Cons" => {Cons _}^C,
//   "Nothing" => {Nothing}^C,
//   "Just" => {Just _}^C,
//   "mul" => {mul _ _}^K,
//   "sub" => {sub _ _}^K,
//   "add" => {add _ _}^K
//   "Anon_Struct_1" => {Anon_Struct_1 _ _ _ _}^C,
//   "Tpl2" => {Tpl2 _ _}^C
// )
#[macro_export]
macro_rules! context {
    ({$runtime:expr} $($key:expr => {$($inside:tt)+}^$kind:ident),*) => ({
        let mut fresh_context = $crate::eval::taint::Context::default();
        $(
            let val = context!(@inside $kind $($inside)+ ; $runtime);
            fresh_context.insert($key.into(), val);
        )+
        fresh_context
    });
    (@inside Unk $what:expr ; $runtime:expr) => ({
        $crate::eval::taint::Value::Unk($what)
    });
    (@inside K $name:ident _ ; $runtime:expr) => ({
        context!(@partial $runtime ; partial!(_x1 / K $name))
    });
    (@inside K $name:ident _ _ ; $runtime:expr) => ({
        context!(@partial $runtime; partial!(_x1 _x2 / K $name))
    });
    (@inside K $name:ident _ _ _ ; $runtime:expr) => ({
        context!(@partial $runtime; partial!(_x1 _x2 _x3 / K $name))
    });
    (@inside K $name:ident _ _ _ _ ; $runtime:expr) => ({
        context!(@partial $runtime; partial!(_x1 _x2 _x3 _x4 / K $name))
    });
    (@inside C $name:ident ; $runtime:expr) => ({
        $crate::eval::taint::Value::Structure {
            constructor: stringify!($name).into(),
            values: Vec::new(),
        }
    });
    (@inside C $name:ident _ ; $runtime:expr) => ({
        context!(@partial $runtime; partial!(_x1 / C $name))
    });
    (@inside C $name:ident _ _ ; $runtime:expr) => ({
        context!(@partial $runtime; partial!(_x1 _x2 / C $name))
    });
    (@inside C $name:ident _ _ _ ; $runtime:expr) => ({
        context!(@partial $runtime; partial!(_x1 _x2 _x3 / C $name))
    });
    (@inside C $name:ident _ _ _ _ ; $runtime:expr) => ({
        context!(@partial $runtime; partial!(_x1 _x2 _x3 _x4 / C $name))
    });
    (@partial $runtime:expr ; $($builder:tt)*) => ({
        let builder = $($builder)*;
        $crate::eval::taint::evaluate(
            &(|_,v|v),
            builder,
            $crate::eval::taint::Rc::new($crate::eval::taint::Context::default()),
            $runtime,
        ).unwrap()
    })
}

pub type Runtime<T> = FxHashMap< String, Box<Fn(Vec<Value<T>>) -> Value<T>> >;

#[macro_export]
macro_rules! runtime {
    ($($key:expr => $value:expr),*) => ({
        let mut fresh_runtime = $crate::eval::taint::Runtime::default();
        $( fresh_runtime.insert($key.into(), Box::new($value)); )+
        fresh_runtime
    })
}

// Insert into the context a value deconstructed according to pattern
fn insert_pattern<T: fmt::Debug+Clone>(
    context: &mut Context<T>,
    pattern: RPattern,
    value: Value<T>
) -> Result<(),String> {
    use self::Pattern::*;
    match pattern {
        Literal | Wildcard => Ok(()) ,
        Ident(name) => {
            context.insert(name, value);
            Ok(())
        },
        Constructor { name, arguments } => {
            match value {
                Value::Structure{constructor, values} => {
                    if constructor == name {
                        for (sub_pattern, sub_value)
                        in arguments.into_iter().zip(values.into_iter()) {
                            insert_pattern(context, sub_pattern, sub_value)?
                        }
                        Ok(())
                    } else {
                        Err(format!("pattern constructor {} did not match with \
                                    value constructor {}", name, constructor))
                    }
                },
                Value::Unk(taintedness) => {
                    for sub_pattern in arguments {
                        insert_pattern(
                            context, sub_pattern,
                            Value::Unk(taintedness.clone())
                        )?
                    }
                    Ok(())
                },
                _ =>
                    Err(format!("pattern constructor {} did not match with \
                                value {:?}", name, value)),
            }
        },
    }
}

/// Evaluates a λ-expression using the given context.
pub fn evaluate_path<PreFn, T>(
    preproc: &PreFn,
    expression: RExpression,
    context: Rc<Context<T>>,
    runtime: &Runtime<T>,
    path: Vec<ExPath>,
) -> Result<T2<Value<T>,Vec<ExPath>>,String>
    where PreFn: Fn(Rc<Context<T>>, Vec<ExPath>, Value<T>) -> Value<T>,
          T: Clone + Taint + PartialEq + fmt::Debug
{
    use self::ExPath::*;
    macro_rules! extd { [$inner:expr] => ({
        let mut inner_path = path.clone();
        inner_path.push($inner);
        inner_path
    })}
    use self::Expression::*;
    use self::Value::{Closure,ReClosure};
    let T2(value,path) = match expression {
        Lambda { binding, box body } =>
            Ok(T2(Closure { context: context.clone(), binding, body }, path)),
        Application {box function, box argument} => {
            let function_value =
                evaluate_path(preproc, function, context.clone(), runtime, extd![AppFun])?;
            let argument_value =
                evaluate_path(preproc, argument, context.clone(), runtime, extd![AppArg])?;
            function_value.apply_path(preproc, argument_value, runtime, path)
        },
        Ident(ref name) =>
            match context.get(name) {
                None => Err(format!("attempt to access innexistant free \
                                    variable {}", name)),
                Some(val) => Ok(T2((*val).clone(),path)),
            },
        Literal =>
            Ok(T2(Value::Unk(T::const_()), path)),
        Letrec { rec_name, binding, box body, box over } => {
            let mut new_context = Context::clone(&context);
            let evaluated = ReClosure {
                binding, body,
                context: context.clone(),
                rec_name: rec_name.clone()
            };
            new_context.insert(rec_name, evaluated);
            evaluate_path(preproc, over, Rc::new(new_context), runtime, extd![LetIn])
        },
        Let { binding, box value, box over } => {
            let mut new_context = Context::clone(&context);
            let evaluated = evaluate_path(preproc, value, context.clone(), runtime, extd![LetLet])?;
            insert_pattern(&mut new_context, binding, evaluated.0)?;
            evaluate_path(preproc, over, Rc::new(new_context), runtime, extd![LetIn])
        },
        Case { box over, branches } => {
            let over_value = evaluate_path(preproc, over, context.clone(), runtime, extd![CaseOver])?;
            let mut matched_branches = match_branches(&over_value.0, branches);
            let tmp_path = path.clone();
            let evaluate_branch = |(i, pat,expr)| {
                let mut branch_context = Context::clone(&context);
                insert_pattern(&mut branch_context, pat, over_value.0.clone())?;
                let mut inner_path = tmp_path.clone();
                inner_path.push(CaseBranch(i));
                evaluate_path(preproc, expr, Rc::new(branch_context),
                    runtime, inner_path)
            };
            let mut final_value = matched_branches.pop().ok_or(
                format!("no matching branches for case expression")
            ).map(|(k,v)|(0u8,k,v)).and_then(evaluate_branch)?.0;

            for (i,(pat,expr)) in matched_branches.into_iter().enumerate() {
                let evaluated = evaluate_branch(((i+1) as u8, pat, expr))?.0;
                final_value = final_value.union(evaluated);
            }
            Ok(T2(final_value,path))
        },
        Constr { name, arguments } => {
            let values = arguments.iter()
                .map(|arg|
                    context.get(arg).ok_or(
                        format!("{} not found in scope", name)
                    ).map(|x| x.clone())
                ).collect::<Result<Vec<Value<T>>,String>>()?;
            Ok(T2(Value::Structure{ constructor: name, values }, path))
        },
        Kernel { name, arguments } => {
            let values = arguments.iter()
                .map(|arg|
                    context.get(arg).ok_or(
                        format!("{} not found in scope", name)
                    ).map(|x| x.clone())
                ).collect::<Result<Vec<Value<T>>,String>>()?;
            let kernel_fillin = runtime.get(&name).ok_or(
                format!("Kernel name not found: {}", name))?;
            Ok(T2((kernel_fillin)(values), path))
        },
    }?;
    Ok(T2((*preproc)(context, path.clone(), value), path))
}

/// Evaluates a λ-expression using the given context.
pub fn evaluate<PreFn, T>(
    preproc: &PreFn,
    expression: RExpression,
    context: Rc<Context<T>>,
    runtime: &Runtime<T>,
) -> Result<Value<T>,String>
    where PreFn: Fn(Rc<Context<T>>, Value<T>) -> Value<T>,
          T: Clone + Taint + PartialEq + fmt::Debug
{
    use self::Expression::*;
    use self::Value::{Closure,ReClosure};
    let value = match expression {
        Lambda { binding, box body } =>
            Ok(Closure { context: context.clone(), binding, body }),
        Application {box function, box argument} => {
            let function_value =
                evaluate(preproc, function, context.clone(), runtime)?;
            let argument_value =
                evaluate(preproc, argument, context.clone(), runtime)?;
            function_value.apply(preproc, argument_value, runtime)
        },
        Ident(ref name) =>
            match context.get(name) {
                None => Err(format!("attempt to access innexistant free \
                                    variable {}", name)),
                Some(val) => Ok((*val).clone()),
            },
        Literal =>
            Ok(Value::Unk(T::const_())),
        Letrec { rec_name, binding, box body, box over } => {
            let mut new_context = Context::clone(&context);
            let evaluated = ReClosure {
                binding, body,
                context: context.clone(),
                rec_name: rec_name.clone()
            };
            new_context.insert(rec_name, evaluated);
            evaluate(preproc, over, Rc::new(new_context), runtime)
        },
        Let { binding, box value, box over } => {
            let mut new_context = Context::clone(&context);
            let evaluated = evaluate(preproc, value, context.clone(), runtime)?;
            insert_pattern(&mut new_context, binding, evaluated)?;
            evaluate(preproc, over, Rc::new(new_context), runtime)
        },
        Case { box over, branches } => {
            let over_value = evaluate(preproc, over, context.clone(), runtime)?;
            let mut matched_branches = match_branches(&over_value, branches);
            let evaluate_branch = |(pat,expr)| {
                let mut branch_context = Context::clone(&context);
                insert_pattern(&mut branch_context, pat, over_value.clone())?;
                evaluate(preproc, expr, Rc::new(branch_context), runtime)
            };
            let mut final_value = matched_branches.pop().ok_or(
                format!("no matching branches for case expression")
            ).and_then(evaluate_branch)?;

            for (pat,expr) in matched_branches {
                let evaluated = evaluate_branch((pat, expr))?;
                final_value = final_value.union(evaluated);
            }
            Ok(final_value)
        },
        Constr { name, arguments } => {
            let values = arguments.iter()
                .map(|arg|
                    context.get(arg).ok_or(
                        format!("{} not found in scope", name)
                    ).map(|x| x.clone())
                ).collect::<Result<Vec<Value<T>>,String>>()?;
            Ok(Value::Structure{ constructor: name, values })
        },
        Kernel { name, arguments } => {
            let values = arguments.iter()
                .map(|arg|
                    context.get(arg).ok_or(
                        format!("{} not found in scope", name)
                    ).map(|x| x.clone())
                ).collect::<Result<Vec<Value<T>>,String>>()?;
            let kernel_fillin = runtime.get(&name).ok_or(
                format!("Kernel name not found: {}", name))?;
            Ok((kernel_fillin)(values))
        },
    }?;
    Ok((*preproc)(context, value))
}



#[derive(Clone,Copy,PartialEq)]
enum Match { Complete, Partial, Diverges }

fn match_branches<T>(value: &Value<T>, branches: Vec<(RPattern, RExpression)>)
    -> Vec<(RPattern, RExpression)>
{
    use self::Match::*;
    fn match_wedge(rhs: Match, lhs: Match) -> Match {
        match (rhs, lhs) {
            (Diverges, _) => Diverges,
            (_, Diverges) => Diverges,
            (Partial, _) => Partial,
            (_, Partial) => Partial,
            (Complete, Complete) => Complete,
        }
    }

    fn matchness<T>(pattern: &RPattern, value: &Value<T>) -> Match {
        use self::Value::{Unk,Structure};
        use self::Pattern::*;
        match (pattern, value) {
            (Ident(_), _) | (Wildcard, _) => Match::Complete,
            (Literal, Unk(_))             => Match::Partial,
            (Constructor{..}, Unk(_))     => Match::Partial,

            (Constructor{ref name, ref arguments},
             Structure{ref constructor, ref values})
             if name == constructor => {
                 arguments.iter().zip(values.iter())
                     .fold(Match::Complete, |acc,(sub_pat, sub_val)| {
                         let sub_matchness = matchness(sub_pat, sub_val);
                         match_wedge(acc, sub_matchness)
                     })
            },
            (Constructor{..}, Structure{..}) =>
                Match::Diverges,

            _ => panic!("impossible match attempt"),
        }
    }

    let mut ret = Vec::new();
    for (pattern, expr) in branches {
        match matchness(&pattern, &value) {
            Complete => {
                ret.push((pattern,expr));
                break
            },
            Partial => {
                ret.push((pattern,expr));
            },
            Diverges => {},
        }
    }
    ret
}


#[derive(Clone)]
pub enum Value<T> {
    Unk(T),
    Closure {
        context: Rc<Context<T>>,
        binding: String,
        body: RExpression,
    },
    ReClosure {
        context: Rc<Context<T>>,
        rec_name: String,
        binding: String,
        body: RExpression,
    },
    Structure {
        constructor: String,
        values: Vec<Value<T>>,
    },
}

#[macro_export]
macro_rules! implFromValue {
    ($origType:ty => $targetType:ty) => (
impl From<$crate::eval::taint::Value<$origType>>
for $crate::eval::taint::Value<$targetType> {
    fn from(other: $crate::eval::taint::Value<$origType>)
        -> $crate::eval::taint::Value<$targetType>
    {
        use $crate::eval::taint::Value::*;
        match other {
            Unk(taint) => Unk(taint.into()),
            Closure { context, binding, body } => {
                let in_ctx = $crate::eval::taint::Rc::new(
                    (*context).clone().into_iter().map(|(k,v)|(k,v.into()))
                    .collect());
                Closure {
                    context: in_ctx,
                    binding, body
                }
            },
            ReClosure { context, rec_name, binding, body } => {
                let in_ctx = $crate::eval::taint::Rc::new(
                    (*context).clone().into_iter().map(|(k,v)|(k,v.into()))
                    .collect());
                ReClosure {
                    context: in_ctx,
                    binding, body, rec_name
                }
            },
            Structure { constructor, values } => {
                let in_values = values.into_iter().map(|x|x.into())
                    .collect();
                Structure {
                    constructor,
                    values: in_values,
                }
            },
        }
    }
}
    )
}

impl<T:Hash> Hash for Value<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        use self::Value::*;
        match self {
            Unk(taint) => {
                1.hash(state);
                taint.hash(state);
            },
            Closure { binding, body, .. } => {
                2.hash(state);
                binding.hash(state);
                body.hash(state);
            },
            ReClosure { rec_name, binding, body, .. } => {
                3.hash(state);
                rec_name.hash(state);
                binding.hash(state);
                body.hash(state);
            },
            Structure { constructor, values } => {
                4.hash(state);
                constructor.hash(state);
                values.hash(state);
            },
        }
    }
}
impl<T:PartialEq> PartialEq for Value<T> {
    fn eq(&self, other: &Value<T>) -> bool {
        use self::Value::*;
        match (self, other) {
            (Unk(ref ltaint), Unk(ref rtaint)) =>
                ltaint == rtaint,
            (Structure { constructor:lc, values:lvs },
             Structure { constructor:rc, values:rvs }) =>
                lc == rc && rvs == lvs,
            _ =>
                false,
        }
    }
}
impl<T:Eq> Eq for Value<T> {}

pub trait Taint {
    fn const_() -> Self;
    fn union(self, Self) -> Self;
}

#[derive(Clone,Debug,PartialEq)]
pub struct T2<X,Y>(X,Y);

impl<T:Clone+Taint+PartialEq+fmt::Debug> T2<Value<T>,Vec<ExPath>> {
    fn apply_path<PreFn>(
        self,
        preproc: &PreFn,
        argument: T2<Value<T>,Vec<ExPath>>,
        runtime: &Runtime<T>,
        path: Vec<ExPath>,
    ) -> Result<T2<Value<T>,Vec<ExPath>>,String>
        where PreFn: Fn(Rc<Context<T>>, Vec<ExPath>, Value<T>) -> Value<T>
    {
        use self::Value::*;
        match self.0 {
            Unk(taint) => {
                Ok(T2( Unk(taint.union( argument.0.mark() )) , path))
            },
            Closure { context, binding, body } => {
                let mut new_context = Context::clone(&context);
                new_context.insert(binding, argument.0);
                evaluate_path(preproc, body, Rc::new(new_context), runtime, path)
            },
            ReClosure { context, rec_name, binding, body } => {
                let mut new_context = Context::clone(&context);
                new_context.insert(binding, argument.0);
                new_context.insert(rec_name, Unk(T::const_()));
                evaluate_path(preproc, body, Rc::new(new_context), runtime, path)
            },
            Structure { constructor, .. } =>
                Err(format!("A constructor has more value stored than\
                            the structure it is building: {}", constructor)),
        }
    }
}

impl<T:Clone+Taint+PartialEq+fmt::Debug> Value<T> {
    pub fn union(self, rhs: Self) -> Self {
        use self::Value::Structure as St;
        if self == rhs {
            self
        } else if let (
                St{constructor:c1,values:values1},
                St{constructor:c2,values:values2}
            ) = (&self,&rhs) {
                if c1 == c2 {
                    let values1 = values1.clone();
                    let values2 = values2.clone();
                    let c1 = c1.clone();
                    let values_result =
                        values1.into_iter()
                            .zip(values2).map(|(v1,v2)| v1.union(v2))
                            .collect();
                    St { constructor: c1, values: values_result }
                } else { Value::Unk(self.mark().union(rhs.mark())) }
        } else { Value::Unk(self.mark().union(rhs.mark())) }
    }

    pub fn mark(&self) -> T {
        use self::Value::*;
        let union_ = |acc :T, v :Value<T>| acc.union(v.mark());
        match self {
            Unk(taint) => taint.clone(),
            Closure { context, body, .. }
            | ReClosure { context, body, .. } => {
                Context::clone(context).into_iter()
                    .filter(|(name, _)| body.appear_free(name))
                    .map(|(_, value)| value)
                    .fold(T::const_(), union_)
            },
            Structure { values, .. } => {
                values.clone().into_iter().fold(T::const_(), union_)
            },
        }
    }

    fn apply<PreFn>(
        self,
        preproc: &PreFn,
        argument: Value<T>,
        runtime: &Runtime<T>,
    ) -> Result<Value<T>,String>
        where PreFn: Fn(Rc<Context<T>>, Value<T>) -> Value<T>
    {
        use self::Value::*;
        match self {
            Unk(taint) => {
                Ok( Unk(taint.union( argument.mark() )) )
            },
            Closure { context, binding, body } => {
                let mut new_context = Context::clone(&context);
                new_context.insert(binding, argument);
                evaluate(preproc, body, Rc::new(new_context), runtime)
            },
            ReClosure { context, rec_name, binding, body } => {
                let mut new_context = Context::clone(&context);
                new_context.insert(binding, argument);
                new_context.insert(rec_name, Unk(T::const_()));
                evaluate(preproc, body, Rc::new(new_context), runtime)
            },
            Structure { constructor, .. } =>
                Err(format!("A constructor has more value stored than\
                            the structure it is building: {}", constructor)),
        }
    }
    pub fn apply_unk<PreFn>(
        self,
        preproc: &PreFn,
        argument: T,
        runtime: &Runtime<T>,
    ) -> Result<Value<T>,String>
        where PreFn: Fn(Rc<Context<T>>, Vec<ExPath>, Value<T>) -> Value<T>
    {
        Ok(T2(self,Vec::new()).apply_path(preproc, T2(Value::Unk(argument),
            Vec::new()), runtime, Vec::new())?.0)
    }
}
