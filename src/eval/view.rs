use fxhash::FxHashSet;

use super::{Taint, Value};


pub type MsgT = FxHashSet<Value<TaintMsg>>;

#[derive(Debug,Clone,Copy,PartialEq,Hash,Eq)]
pub enum TaintMsg { Const, View }

#[derive(Debug,Clone,PartialEq)]
pub enum TaintView { Const, View, Msg(MsgT) }

impl From<TaintView> for TaintMsg {
    fn from(view: TaintView) -> TaintMsg {
        match view {
            TaintView::Const  => TaintMsg::Const,
            TaintView::View   => TaintMsg::View,
            TaintView::Msg(_) => TaintMsg::Const,
        }
    }
}

impl Taint for TaintView {
    fn const_() -> Self {
        TaintView::Const
    }
    fn union(self, rhs: Self) -> Self {
        use self::TaintView::*;
        match (self, rhs) {
            (Msg(mut lmsg), Msg(rmsg)) => {
                lmsg.extend(rmsg.into_iter());
                Msg(lmsg)
            },
            (_, msg@Msg(_)) | (msg@Msg(_), _) =>
                msg,
            (View, _) | (_, View) => View,
            (Const, Const) => Const,
        }
    }
}

impl Taint for TaintMsg {
    fn const_() -> Self {
        TaintMsg::Const
    }
    fn union(self, rhs: Self) -> Self {
        use self::TaintMsg::*;
        match (self, rhs) {
            (View, _) | (_, View) => View,
            (Const, Const) => Const,
        }
    }
}
