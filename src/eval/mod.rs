pub use self::taint::{Value,Taint};
pub use super::{Expression, Pattern};

pub mod view;
pub mod update;
#[macro_use]
pub mod taint;

implFromValue!(view::TaintView => view::TaintMsg);
