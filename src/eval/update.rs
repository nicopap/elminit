use super::Taint;
use super::view::TaintMsg;

#[derive(Debug,Clone,Copy)]
pub enum TaintUpdate { Const, View, Update }


impl From<TaintMsg> for TaintUpdate {
    fn from(msg: TaintMsg) -> TaintUpdate {
        match msg {
            TaintMsg::Const => TaintUpdate::Const,
            TaintMsg::View => TaintUpdate::View,
        }
    }
}

impl Taint for TaintUpdate {
    fn const_() -> Self {
        TaintUpdate::Const
    }
    fn union(self, rhs: Self) -> Self {
        use self::TaintUpdate::*;
        match (self, rhs) {
            (View, _) | (_, View) => View,
            (Update, _) | (_, Update) => Update,
            (Const, Const) => Const,
        }
    }
}
