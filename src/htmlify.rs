use std::char;
use std::fmt::Display;

use super::expression::{ExPath,Expression};

pub fn classify(path: &[ExPath]) -> String {
    use self::ExPath::*;
    path.iter().map(|path_element| match path_element {
        LBody => 'b',
        AppFun=> 'f',
        AppArg=> 'a',
        LetLet=> 'l',
        LetIn => 'i',
        CaseOver=> 'c',
        CaseBranch(which)=>
            char::from_digit(*which as u32, 36).unwrap()
                .to_ascii_uppercase(),
    }).collect()
}

pub fn to_html<N:Display>(to_render:&Expression<N>) -> String {
    to_html_hp(to_render, Vec::new())
}

fn to_html_hp<N>(
    to_render: &Expression<N>,
    path: Vec<ExPath>
) -> String
    where N: Display,
{
    macro_rules! extd { [$inner:expr] => ({
        let mut inner_path = path.clone();
        inner_path.push($inner);
        inner_path
    })}

    use self::ExPath::*;
    use self::Expression::*;
    match to_render {
        Lambda { ref binding, ref body } =>
            format!("<span id=\"{}\">λ{}.{}</span>",
                    classify(&path), binding, to_html_hp(body, extd![LBody])),

        Application { ref function, ref argument } => {
            let fun_format = match function.as_ref() {
                Lambda {..} | Case {..} | Letrec {..} | Let {..} =>
                    format!("({})", to_html_hp(function, extd![AppFun])),

                Literal| Ident(_)| Application{..}| Constr{..}| Kernel{..} =>
                    format!("{}", to_html_hp(function, extd![AppFun])),
            };
            let arg_format = match argument.as_ref() {
                Lambda{..} | Application{..}
                | Case{..} | Letrec{..} | Let{..} =>
                    format!("({})", to_html_hp(argument, extd![AppArg])),

                Literal | Ident(_) | Constr{..} | Kernel{..} =>
                    format!("{}", to_html_hp(argument, extd![AppArg])),
            };
            format!("<span id=\"{}\">{} {}</span>",
                    classify(&path), fun_format, arg_format)
        },
        Literal =>
            format!("<span id=\"{}\">:lit:</span>", classify(&path)),

        terminal@Ident(_) | terminal@Kernel{..} | terminal@Constr{..} =>
            format!("<span id=\"{}\">{}</span>", classify(&path), terminal),

        Let { ref binding, ref value, ref over } =>
            format!(
                "<div id=\"{}\">let {} = <div>{}</div> in <div>{}</div></div>",
                classify(&path),
                binding,
                to_html_hp(value, extd![LetLet]),
                to_html_hp(over, extd![LetIn])
            ),
        Letrec { ref binding, ref rec_name, ref body, ref over } =>
            format!(
                "<div id=\"{}\">letrec {} = λ {} . <div>{}</div> \
                in <div>{}</div></div>",
                classify(&path),
                rec_name, binding,
                to_html_hp(body, extd![LetLet]),
                to_html_hp(over, extd![LetIn])
            ),
        Case { ref over, ref branches } => {
            let mut branches_format = String::new();
            for (i, (pat, expr)) in branches.iter().enumerate() {
                branches_format += &format!(
                    "{} → <div>{}</div>", pat,
                    to_html_hp(expr, extd![CaseBranch(i as u8)])
                );
            }
            format!("<div id=\"{}\">case {} of <div>{}</div></div>",
                classify(&path),
                to_html_hp(over, extd![CaseOver]),
                branches_format,
            )
        },
    }
}
