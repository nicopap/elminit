#![feature(trace_macros)]

#[macro_use]
extern crate elminit;

use std::env;
use std::fmt;
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashSet;
use elminit::{
    eval::{
        self,
        view::{self,TaintView,MsgT},
        taint::{self,Taint}
    },
    htmlify,
    expression::ExPath
};


const HEADER_INLINE :  &'static str = r##"
<head>
    <meta charset="UTF-8">
    <title>Demo Verification</title>
    <style>body{font-family:monospace;font-size:20px}</style>
</head>
"##;

const JAVASCRIPT_INLINE_CODE : &'static str = r##"
var current_evaluation_line = 0;

function advance_execution(steps) {
  var new_line = current_evaluation_line + steps;
  if (new_line < 0 || new_line >= execution_sheet.length) { return }
  if (execution_sheet[new_line].block === "") { return }

  var old_state = execution_sheet[current_evaluation_line];
  document.getElementById(old_state.block).style.backgroundColor = "#ffffff00";

  current_evaluation_line = new_line;
  var new_state = execution_sheet[new_line];
  document.getElementById(new_state.block).style.backgroundColor = "#60f0f0";
  document.getElementById("showvalue").innerHTML = new_state.value;
}

document.addEventListener('keydown', function(event) {
  var pressed_key = event.key;
  if (pressed_key === "ArrowLeft" || pressed_key === "ArrowRight") {
    event.preventDefault();
    var steps = pressed_key == "ArrowRight" ? 1 : -1;
    advance_execution(steps)
  }
}, true);"##;


#[derive(Debug,Clone)]
struct Tracker {
    path: Vec<ExPath>,
    value: taint::Value<TaintView>,
    marking: TaintView,
}

fn propagate<T>(args: Vec<taint::Value<T>>) -> taint::Value<T>
where T: Taint+Clone+PartialEq+fmt::Debug
{
    args.into_iter()
        .fold(taint::Value::Unk(Taint::const_()),
              |acc,x| acc.union(x))
}
fn extract<T>(args: Vec<taint::Value<T>>) -> taint::Value<T>
where T: Taint+Clone+PartialEq+fmt::Debug
{
    let (update,view) = match args.get(0) {
        Some(taint::Value::Structure { constructor, values }) =>
            if constructor == "Anon_Struct_1" {
                (values.get(1).unwrap().clone()
                , values.get(2).unwrap().clone()
                )
            } else {
                panic!("Tried to `program` on incompatible type")
            }
        _ =>
            panic!("Tried to `program` on incompatible type"),
    };
    taint::Value::Structure {
        constructor: "Tpl2".into(),
        values: vec![update,view],
    }
}
fn collect(args: Vec<taint::Value<TaintView>>)
    -> taint::Value<TaintView>
{
    let arg = args.into_iter().next().unwrap();
    let mut singleton = MsgT::default();
    singleton.insert(arg.into());
    taint::Value::Unk(TaintView::Msg(singleton))
}

fn extract_prog(value: taint::Value<TaintView>)
    -> (taint::Value<TaintView>,taint::Value<TaintView>)
{
    match value {
        taint::Value::Structure { constructor, values } =>
            if constructor == "Tpl2" {
                ( values.get(0).unwrap().clone()
                , values.get(1).unwrap().clone()
                )
            } else {
                panic!("Tried to `program` on incompatible type")
            }
        _ =>
            panic!("Tried to `program` on incompatible type"),
    }
}

#[derive(PartialEq, Debug)]
enum ProgArg { Fib, List, Demo }

pub fn main() {
    let parse_arg = |arg:String| match arg.as_ref() {
        "-f" => ProgArg::Fib,
        "-e" => ProgArg::Demo,
        _    => ProgArg::List,
    };
    let arg = env::args().map(parse_arg).nth(1).unwrap_or(ProgArg::List);

    let runtime = runtime!(
        "Elm_Kernel_mul" => &propagate,
        "Elm_Kernel_sub" => &propagate,
        "Elm_Kernel_add" => &propagate,
        "Elm_Kernel_program" => &extract,
        "Elm_Kernel_onClick" => &collect,
        "Elm_Kernel_Time_every" => &propagate,
        "Elm_Kernel_div" => &propagate,
        "Elm_Kernel_append" => &propagate,
        "Elm_Kernel_Html_text" => &propagate
    );
    let context : taint::Context<eval::view::TaintView> = context!(
        {&runtime}
            "False" => {False}^C,
            "True" => {True}^C,
            "Nil" => {Nil}^C,
            "Cons" => {Cons _ _}^C,
            "Nothing" => {Nothing}^C,
            "Just" => {Just _}^C,
            "mul" => {Elm_Kernel_mul _ _}^K,
            "sub" => {Elm_Kernel_sub _ _}^K,
            "add" => {Elm_Kernel_add _ _}^K,
            "append" => {Elm_Kernel_append _ _}^K,
            "H_program" => {Elm_Kernel_program _}^K,
            "text" => {Elm_Kernel_Html_text _}^K,
            "Anon_Struct_1" => {Anon_Struct_1 _ _ _ _}^C,
            "onClick" => {Elm_Kernel_onClick _}^K,
            "div" => {Elm_Kernel_div _ _}^K,
            "Time_every" => {Elm_Kernel_Time_every _}^K,
            "SetClick" => {SetClick _}^C,
            "IncrementBoth" => {IncrementBoth}^C,
            "ToggleAuto" => {ToggleAuto}^C,
            "Tpl2" => {Tpl2 _ _}^C,
            "Model" => {Model _ _ _}^C,
            "Cmd_none" => {Cmd_none}^C,
            "taint" => {eval::view::TaintView::View}^Unk,
            "clean" => {eval::view::TaintView::Const}^Unk,
            "button" => {eval::view::TaintView::Const}^Unk,
            "toString" => {eval::view::TaintView::Const}^Unk,
            "h1" => {eval::view::TaintView::Const}^Unk
    );

    let elminit_expr : elminit::Expression<String> = match arg {
        ProgArg::List => {
            elminit!(
                letrec map = λ f . { λ il .
                    case (il) of {
                        (^Nil) -> { Nil },
                        (^Cons (h) (t)) -> {Cons (f h) (map f t)}
                    }
                } in letrec tail = λ list . {
                    case (list) of {
                        (^Nil) -> {Nothing},
                        (^Cons (t) (^Nil)) -> {Just t},
                        (^Cons (_) (t)) -> {tail t}
                    }
                } in letrec header = λ list . {
                    case (list) of {
                        (^Nil) -> {Nil},
                        (^Cons (h) (^Cons (_) (^Nil))) -> {Cons h Nil},
                        (^Cons (h) (t)) -> {Cons h (header t)}
                    }
                } in let (ml) = { Cons clean (Cons clean (Cons taint Nil)) }
                in let (dl) = { map (mul 3)}
                in tail (header (dl ml))
            )
        },
        ProgArg::Fib => {
            elminit!(
                letrec fib = λ n . {
                    case (n) of {
                        (0) -> { 0 },
                        (1) -> { 1 },
                        (n) -> { add (fib (sub n clean)) (fib (sub n clean)) }
                    }
                } in fib taint
            )
        },
        ProgArg::Demo => {
            include!("html_elmini-democode.elm")
        },
    };

    let tracker_view_cell = RefCell::new(Vec::new());
    let tracker_view =
        |ctx: Rc<taint::Context<_>>, mut path: Vec<ExPath>, val: taint::Value<_>|
            -> taint::Value<_>
        {
            use self::ExPath::*;
            let mut tracker = tracker_view_cell.borrow_mut();
            let mut path_real = vec![LetIn,LetIn,LetLet,LBody];
            path_real.append(&mut path);
            tracker.push(Tracker {
                path: path_real,
                value: val.clone(),
                marking: val.mark(),
            });
            val
        };
    let result = extract_prog(taint::evaluate(
        &(|_,v|v),
        elminit_expr.clone(),
        Rc::new(context),
        &runtime,
    ).unwrap());

    let view_value =
        result.1.clone().apply_unk(&tracker_view, TaintView::View, &runtime);

    println!("{}", HEADER_INLINE);
    println!("<script>\nvar execution_sheet = [");
    for t in tracker_view_cell.borrow().iter() {
        println!("   {{block: \"{}\", value: `{:?}`}},",
                 htmlify::classify(&t.path), &t.marking)
    }
    println!("];{}</script>", JAVASCRIPT_INLINE_CODE);
    println!("<style>div{{margin-left:8px}}</style>");
    println!("<body>{}", htmlify::to_html(&elminit_expr));
    println!("<div id=\"showvalue\"></div></body>");
}
