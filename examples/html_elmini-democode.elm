elminit!(
let
  (init) = {Model 0 0 False}
in let
  (subscriptions) = { λ m . let (^Model (_) (_) (autoclick)) = {m} in
    case (not autoclick) of {
      (^True) -> {
        Sub_none
      },
      (^False) -> {
        Time_every (mul 30 Time_millisecond) (λ x . IncrementBoth)
      }
    }
  }
in let
  (view) = { λ x . let (^Model (derivedUpdate) (noDerive) (_)) = { x } in
    div Nil
      (Cons (button
        (Cons (onClick (SetClick (add derivedUpdate 1))) Nil)
        (Cons (text "incrément") Nil)
      )
      (Cons
        (h1 Nil (Cons (text (append "valeur dérivée: " (toString derivedUpdate))) Nil))
      (Cons
        (h1 Nil (Cons (text (append "valeur contrôle: " (toString noDerive))) Nil))
      (Cons
        (button (Cons (onClick ToggleAuto) Nil) (Cons (text "auto") Nil))
        Nil))))
  }
in let
  (update) = { λ msg . λ model .
    let (^Model (derivedUpdate) (noDerive) (autoclick)) = {model} in
    let (model_updated) = {
      case (msg) of {
        (^SetClick (newCount)) -> {
          Model newCount (add noDerive 1) autoclick
        },
        (^IncrementBoth) -> {
          Model
            (add derivedUpdate 1)
            (add noDerive 1)
            autoclick
        },
        (^ToggleAuto) -> {
          Model
            (get_derivedUpdate model)
            (get_noDerive model)
            (not autoclick)
        }
      }
    } in
      Tpl2 model_updated Cmd_none
  }
in
  H_program (Anon_Struct_1
    (Tpl2 init Cmd_none)
    (update) (view) (subscriptions)
  )
)
